<?php
namespace app\components\yii2ShowActions;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

/*

this plugin designed to show controller actions and arguments in yii 2.0

example view file
	
	use app\components\yii2ShowActions\ShowActions;
	ShowActions::widget(['controller'=>$this->context])	
	
*/


class ShowActions extends Widget
{
    public $message;
    public $controller;
    public $reflection;
    public $controllerComment = null;


	public function getActions($controller){
	  $reflection = new \ReflectionClass($controller);
	  $this->controllerComment = self::cleanComment($reflection->getDocComment());
	  $methods = $reflection->getMethods();
	  $out = array();
	  $i = 0;
		foreach($methods as $method){
			if(!$method->name){continue;}
			if(!strstr($method->name, 'action') || strstr($method->name, 'actions')){continue;}
			$actionName = str_ireplace('action', '', $method->name); 
			$out[$i]['action'] = Html::a($actionName, [$controller->id.'/'.lcfirst($actionName)]);
			$out[$i]['parameters'] = self::getMethodParameters($method);
			$out[$i]['comments'] = self::getMethodComments($method);
			$i++;
			
		}
	return $out;
	}
	
	public function cleanComment($comment){
		return str_ireplace(array('/**', '/*', '**/', '*/'), '', $comment);
	}
	
	public function getMethodComments($method){
		//return 'comment';
		//var_dump($method->getDocComment());
		return self::cleanComment($method->getDocComment());
	}
	
	public function getMethodParameters($method){
		$array = array();
		$parameters = $method->getParameters();
		if(!empty($parameters)){
			foreach($parameters as $parameter){
				if(!$parameter->name){continue;}
				$array[] = '$'.$parameter->name;
			}
		}
	return $array;
	}
	

    public function init()
    {
        parent::init();
        if ($this->message === null) {
            $this->message = 'Hello World';
        }
    }

    public function run()
    {
	 $actions = self::getActions($this->controller);
	 $dataProvider = new \yii\data\ArrayDataProvider([
				//'key'=>'CommonName',
				'allModels'=>$actions,				    
				    'sort' => [
					'attributes' => ['name', 'action'],
					],
			]);
 
        //return Html::encode($this->message);
		return $this->render('actions', array('controller'=>$this->controller, 'message'=>$this->message, 'actions'=>$actions, 'dp'=>$dataProvider, 'comment'=>$this->controllerComment));
    }
}