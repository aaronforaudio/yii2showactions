This plugin designed to show controller actions and arguments in a gridview.  Compatible with yii 2.0

## Installation: ##
Copy or clone to app/components/yii2ShowActions

## Comments: ##
Doc comments and method comments will be shown in the grid view.  Doc comments must fall AFTER namespace, and method comments should be just before the method.  Comments begin with '/**' and end with '**/'.  Examples below:

```php
<?php

namespace app\controllers;
/**
<p>Turn a Samsung tv on and off, as well as poll power status and change inputs using hdmi-cec.
This class relies on cec-client being installed, and the batch file located at components/tv.sh being linked to /bin/tv, or otherwise executable at the command line by running 'tv.'</p>
**/

use Yii;

...

class TvController extends MyController
{
	
    public $defaultAction = 'about';


    /**
    turns the tv on
    **/  
    public function actionOn()
    {
		$output = shell_exec("tv on");
		#$boolean = (bool)( strstr($output, 'alive') );
	echo $output;
    }

...

```

## Example use: ##
In this example, $this is the view, and $this->context is the controller.  The following is a complete view file:

```php
<?php	
	use app\components\yii2ShowActions\ShowActions;
	echo ShowActions::widget(['controller'=>$this->context]);	
?>
```

## Example Screenshot: ##
![Screenshot 2016-02-16 16.25.30.png](https://bitbucket.org/repo/KxgyXL/images/3431772645-Screenshot%202016-02-16%2016.25.30.png)
