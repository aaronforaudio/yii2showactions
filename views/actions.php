<h2><?= $controller->className(); ?></h2>


<?php //var_dump($controller);?>

<?php if(!empty($comment)):?>
<h4>Comments:</h4>
<div class="well">
<p><?= $comment ?></p>
</div>
<?php endif; ?>

<h4>Available Actions:</h4>
<?php 
echo yii\grid\GridView::widget([
    'dataProvider' => $dp,
    'columns' => [['attribute'=>'action', 'label'=>'Action/Url','format'=>'html' ,'value'=>function($data){return $data['action'];}], array('header'=>'Parameters', 'value'=>function($data){return implode(', ', $data['parameters']);}), 'comments'],
    'layout'=>"{items}\n{pager}{summary}\n",
    //'filterModel'=>$form,
    //'summary'=>'showing <b>{begin}-{end}</b> of <b>{totalCount}</b> <i>(last updated: '.trim($status['updated']).')</i>',
   // 'columns' => ['CommonName']
]);
?>